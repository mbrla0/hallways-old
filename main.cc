//
//  main.cpp
//  Hallways
//
//  Created by Matheus Borella on 14/07/17.
//  Copyright © 2017 Matheus Borella. All rights reserved.
//

#include <cctype>
#include <cstring>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

#include <chrono>
#include <thread>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#define WINDOW_TITLE  "Hallways"
#define WINDOW_WIDTH  320
#define WINDOW_HEIGHT 200
#define WINDOW_TARGET_FPS 60

#define PC_DISPLAY_WIDTH  320
#define PC_DISPLAY_HEIGHT 200

#include "palette.h"
#include "math.hh"
//#include "raster.hh"

namespace araw{
	struct Header{ uint32_t width; uint32_t height; };

	struct image{
		Header header;
		std::vector<uint8_t> data;

		image(std::istream& in){
			// Read header
			in.read((char*) &this->header, sizeof(Header));
			if(in.fail())
				throw std::runtime_error("Could not read header");

			this->data.resize(this->header.width * this->header.height, 0);
			in.read((char*) this->data.data(), this->header.width * this->header.height);
			if(in.fail())
				throw std::runtime_error("Could not read image data");
		}
	};
}

namespace map{

}

namespace PC{
	sf::Color *palette = nullptr;
	uint8_t   *display = nullptr;


	sf::RenderWindow window;
	void Init(){
		window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT),
					  WINDOW_TITLE,
					  sf::Style::Resize);
		window.setVisible(true);
		window.setActive();
		window.setView(sf::View({(float) PC_DISPLAY_WIDTH / 2.0f, (float) PC_DISPLAY_HEIGHT / 2.0f}, {PC_DISPLAY_WIDTH, PC_DISPLAY_HEIGHT}));

		// Setup display
		display = (uint8_t*) std::malloc(PC_DISPLAY_WIDTH * PC_DISPLAY_HEIGHT);
		if(!display)
			throw std::runtime_error("Cold not allocate display buffer.");

		// Setup palette
		palette = (sf::Color*) std::malloc(0x100 * sizeof(sf::Color));
		if(!palette){
			std::cerr << "Unable to allocate palette memory, using STANDARD_VGA_PALETTE as a fallback." << std::endl;
			palette = STANDARD_VGA_PALETTE;
		}

		// Clear palette and screen
		std::memset(palette, 0, 0x100 * sizeof(sf::Color));
		std::memset(display, 0, PC_DISPLAY_WIDTH * PC_DISPLAY_HEIGHT);

		std::ifstream pal_file("Resources/Palettes/standard.pal", std::ios_base::in | std::ios_base::binary);
		if(pal_file){
			pal_file.read((char*) palette, 0x100 * sizeof(sf::Color));
			if(pal_file.fail()){
				std::cerr << "Couldn't load palette file, using STANDARD_VGA_PALETTE as a fallback." << std::endl;
				palette = STANDARD_VGA_PALETTE;
			}

			pal_file.close();
		}else{
			std::cerr << "Couldn't open palette file, using STANDARD_VGA_PALETTE as a fallback." << std::endl;
			palette = STANDARD_VGA_PALETTE;
		}
	}

	void Run(void(*callback)(void)){
		while(window.isOpen()){
			// Time execution
			using std::chrono::steady_clock;
			std::chrono::time_point<steady_clock> start = steady_clock::now();

			callback();

			//
			// Build Frame
			//
			sf::Texture texture;
			texture.create(PC_DISPLAY_WIDTH, PC_DISPLAY_HEIGHT);

			// Use the pallete to build a full color array
			sf::Color *image = (sf::Color*) malloc(PC_DISPLAY_WIDTH * PC_DISPLAY_HEIGHT * sizeof(sf::Color));
			if(!image)
				throw std::runtime_error("Unable to allocate frame buffer");

			for(size_t i = 0; i < PC_DISPLAY_WIDTH * PC_DISPLAY_HEIGHT; ++i){
				image[i] = palette[display[i]];
			}

			texture.update((uint8_t*) image);

			free(image);

			// Bind the texture to a sprite and render it
			sf::Sprite frame(texture);

			window.clear();
			window.draw(frame);
			window.display();

			// Cap framerate
			std::chrono::time_point<steady_clock> end = steady_clock::now();
			std::cout << "Frame latency: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" << std::endl;
			if(end - start < std::chrono::milliseconds(1000 / WINDOW_TARGET_FPS)){
				std::this_thread::sleep_for(std::chrono::milliseconds(1000 / WINDOW_TARGET_FPS) - (end - start));
			}
		}
	}

	uint8_t *PixelAt(int x, int y){
		// Clip ofscreen pixels
		if(y >= PC_DISPLAY_HEIGHT || y < 0 || x >= PC_DISPLAY_WIDTH || x < 0)
			return nullptr;

		if(y * PC_DISPLAY_WIDTH + x < PC_DISPLAY_WIDTH * PC_DISPLAY_HEIGHT && y * PC_DISPLAY_WIDTH + x >= 0)
			return &display[y * PC_DISPLAY_WIDTH + x];
		else
			return nullptr;
	}

	void Plot(unsigned int x, unsigned int y, uint8_t color){
		if(y < PC_DISPLAY_WIDTH && x < PC_DISPLAY_WIDTH
			&& y * PC_DISPLAY_WIDTH + x < PC_DISPLAY_WIDTH * PC_DISPLAY_HEIGHT)
			display[y * PC_DISPLAY_WIDTH + x] = color;
	}

	void set_color(uint8_t index, sf::Color color){
		palette[index] = color;
	}
}

int sign(int a){
	if(a >= 0)
		return 1;
	else
		return -1;
}

struct Point2{
	float x, y;

	void Plot(uint8_t color){
		uint8_t *pixel = PC::PixelAt((int) std::round(this->x), (int) std::round(this->y));
		if(pixel)
			*pixel = color;
	}

	void operator *=(const Matrix4<float>& matrix){
		Matrix4<float> tmp;
		tmp[0][3] = this->x;
		tmp[1][3] = this->y;
		tmp[2][3] = 1;
		tmp[3][3] = 1;

		tmp *= matrix;

		this->x = tmp[0][3];
		this->y = tmp[1][3];
	}
};

void FillRectangle(float min_x, float min_y, float max_x, float max_y, uint8_t color){
	for(size_t y = std::round(min_y); y < std::round(max_y); ++y){
		if(PC::PixelAt(std::round(min_x), (int) y) == nullptr) continue;

		memset(PC::PixelAt(std::round(min_x), (int) y), color, std::round(max_x) - std::round(min_x));
	}
}

void DrawLine(Point2 p1, Point2 p2, uint8_t color){
	int dx = std::round(p2.x - p1.x);
	int dy = std::round(p2.y - p1.y);

	p1.Plot(color);
	if(std::abs(dx)	>= std::abs(dy)){
		float y = std::abs(dy) * 2;
		for(int i = 0; i < std::abs(dx); ++i){
			y += std::abs(dy);
			if(y >= std::abs(dx)){
				y -= std::abs(dx);
				p1.y += sign(dy);
			}

			p1.x += sign(dx);
			p1.Plot(color);
		}
	}else{
		float x = std::abs(dx) * 2;
		for(int i = 0; i < std::abs(dy); ++i){
			x += std::abs(dx);
			if(x >= std::abs(dy)){
				x -= std::abs(dy);
				p1.x += sign(dx);
			}

			p1.y += sign(dy);
			p1.Plot(color);
		}
	}
}

struct Line{
	Point2 p1, p2;

	const float dx() const {
		return this->p2.x - this->p1.x;
	}

	const float dy() const {
		return this->p2.y - this->p1.y;
	}

	void Plot(uint8_t color){
		DrawLine( this->p1, this->p2, color);
	}

	void operator *=(const Matrix4<float>& matrix){
		this->p1 *= matrix;
		this->p2 *= matrix;
	}

	float space(const Point2& p) const{
		return (this->p2.x - this->p1.x) * (p.y - this->p1.y) - (this->p2.y - this->p1.y) * (p.x - this->p1.x);
	}
};

template <typename fixed_point>
void FillFixedRectangle(fixed_point min_x, fixed_point min_y, fixed_point max_x, fixed_point max_y, uint8_t color){
	int min_xr, min_yr, max_xr, max_yr;
	min_xr = min_x.round();
	min_yr = min_y.round();
	max_xr = max_x.round();
	max_yr = max_y.round();

	// Clip boundaries to display
	if(max_xr >= PC_DISPLAY_WIDTH)  max_xr = PC_DISPLAY_WIDTH;
	if(max_yr >= PC_DISPLAY_HEIGHT) max_yr = PC_DISPLAY_HEIGHT;
	if(max_xr < 0) max_xr = 0;
	if(max_yr < 0) max_yr = 0;
	if(min_yr < 0) min_yr = 0;
	if(min_xr < 0) min_xr = 0;

	for(size_t y = min_yr; y < max_yr; ++y){
		uint8_t *row = PC::PixelAt(min_xr, (int) y);
		if(row != nullptr)
			std::memset(row, color, max_xr - min_xr);
	}
}

void DrawTriangle(Point2 p1_f, Point2 p2_f, Point2 p3_f, uint8_t color, bool back_culling = false){
	#define BLOCK_SIZE 8

	// Convert to fixed-point types
	typedef FixedPoint<int64_t, 32> fixed_point;
	struct Point{
		fixed_point x, y;

		void Plot(uint8_t color){
			PC::Plot((int) x.round(), (int) y.round(), color);

		}
	};

	struct Line{
		Point p1, p2;

		const fixed_point dx() const {
			return this->p2.x - this->p1.x;
		}

		const fixed_point dy() const {
			return this->p2.y - this->p1.y;
		}

		fixed_point space(const Point& p) const{
			return (this->p2.x - this->p1.x) * (p.y - this->p1.y) - (this->p2.y - this->p1.y) * (p.x - this->p1.x);
		}
	};

	Point p1 = {p1_f.x, p1_f.y};
	Point p2 = {p2_f.x, p2_f.y};
	Point p3 = {p3_f.x, p3_f.y};

	// Bounding rectangle
	struct {
		 fixed_point min_x, min_y, max_x, max_y;
	} bounds;
	bounds = {
		std::min(p1.x, std::min(p2.x, p3.x)),
		std::min(p1.y, std::min(p2.y, p3.y)),
		std::max(p1.x, std::max(p2.x, p3.x)),
		std::max(p1.y, std::max(p2.y, p3.y)),
	};

	bounds.max_x += BLOCK_SIZE - (int) (bounds.max_x - bounds.min_x).round() % BLOCK_SIZE;
	bounds.max_y += BLOCK_SIZE - (int) (bounds.max_y - bounds.min_y).round() % BLOCK_SIZE;

	// Initially, asume the triangle is front-facing
	Line e1, e2, e3;
	e1 = {p1, p2};
	e2 = {p2, p3};
	e3 = {p3, p1};

	// Then count the number of times the edges point in each directios
	struct {
		uint8_t clockwise = 0;
		uint8_t counter   = 0;

		int y = 0;
		int x = 0;

		void take(const Line& e){
			if(x != 0 && e.dy() != (long) 0){
				fixed_point dy = e.dy();
				if(dy > 0 && x > 0){
					y = 1;
					++counter;
				}else if(dy > 0 && x < 0){
					y = 1;
					++clockwise;
				}else if(dy < 0 && x > 0){
					y = -1;
					++clockwise;
				}else if(dy < 0 && x < 0){
					y = -1;
					++counter;
				}

				x = 0;
			}else if(y != 0 && e.dx() != (long) 0){
				fixed_point dx = e.dx();
				if(dx > 0 && y > 0){
					x = 1;
					++clockwise;
				}else if(dx > 0 && y < 0){
					x = 1;
					++counter;
				}else if(dx < 0 && y > 0){
					x = -1;
					++counter;
				}else if(dx < 0 && y < 0){
					x = -1;
					++clockwise;
				}

				y = 0;
			}else{
				if(e.dx() > e.dy()){
					x = e.dx().signum();
				}else if(e.dx() < e.dy()){
					y = e.dy().signum();
				}
			}
		}
	} clock;

	clock.take(e1);
	clock.take(e2);
	clock.take(e3);

	// if(e1.dy() > e) ++up; else if(e1.dy() < 0) ++down;
	// if(e2.dy() > 0) ++up; else if(e2.dy() < 0) ++down;
	// if(e3.dy() > 0) ++up; else if(e3.dy() < 0) ++down;
	//
	// if(e1.dx() > 0) ++right; else if(e1.dx() < 0) ++left;
	// if(e2.dx() > 0) ++right; else if(e2.dx() < 0) ++left;
	// if(e3.dx() > 0) ++right; else if(e3.dx() < 0) ++left;

	// And, using that check if the triangle is back-facing
	if(clock.clockwise > clock.counter/*up == down ? right < left : up < down*/){
		printf("Backfacing!\n");
		// Back-facing triangle
		if(back_culling)
			// If we're not drawing backfaces,
			// we can safely end the function here
			return;

		// In order for a triangle to be rendered using half-space,
		// its vertices should be in a counter-clockwise order, in
		// other words, it must be front-facing. To render them, we
		// simply invert the vertex order when creating the edges.
		e1 = {p2, p1};
		e2 = {p1, p3};
		e3 = {p3, p2};
	}

	fixed_point deltax_e1 = e1.p2.x - e1.p1.x;
	fixed_point deltay_e1 = e1.p2.y - e1.p1.y;
	fixed_point deltax_e2 = e2.p2.x - e2.p1.x;
	fixed_point deltay_e2 = e2.p2.y - e2.p1.y;
	fixed_point deltax_e3 = e3.p2.x - e3.p1.x;
	fixed_point deltay_e3 = e3.p2.y - e3.p1.y;

	fixed_point block_deltax_e1 = deltax_e1 * BLOCK_SIZE;
	fixed_point block_deltay_e1 = deltay_e1 * BLOCK_SIZE;
	fixed_point block_deltax_e2 = deltax_e2 * BLOCK_SIZE;
	fixed_point block_deltay_e2 = deltay_e2 * BLOCK_SIZE;
	fixed_point block_deltax_e3 = deltax_e3 * BLOCK_SIZE;
	fixed_point block_deltay_e3 = deltay_e3 * BLOCK_SIZE;

	Point tl = { bounds.min_x,              bounds.min_y };
	Point tr = { bounds.min_x + BLOCK_SIZE, bounds.min_y };
	Point bl = { bounds.min_x,              bounds.min_y + BLOCK_SIZE };
	Point br = { bounds.min_x + BLOCK_SIZE, bounds.min_y + BLOCK_SIZE };

	fixed_point tl_space_e1 = e1.space(tl);
	fixed_point tl_space_e2 = e2.space(tl);
	fixed_point tl_space_e3 = e3.space(tl);

	fixed_point tr_space_e1 = e1.space(tr);
	fixed_point tr_space_e2 = e2.space(tr);
	fixed_point tr_space_e3 = e3.space(tr);

	fixed_point bl_space_e1 = e1.space(bl);
	fixed_point bl_space_e2 = e2.space(bl);
	fixed_point bl_space_e3 = e3.space(bl);

	fixed_point br_space_e1 = e1.space(br);
	fixed_point br_space_e2 = e2.space(br);
	fixed_point br_space_e3 = e3.space(br);

	size_t block_count_x = (bounds.max_x - bounds.min_x).round() / BLOCK_SIZE;
	size_t block_count_y = (bounds.max_y - bounds.min_y).round() / BLOCK_SIZE;
	for(size_t bx = 0; bx < block_count_x; ++bx){
		fixed_point tl_subspace_e1 = tl_space_e1;
		fixed_point tl_subspace_e2 = tl_space_e2;
		fixed_point tl_subspace_e3 = tl_space_e3;

		fixed_point tr_subspace_e1 = tr_space_e1;
		fixed_point tr_subspace_e2 = tr_space_e2;
		fixed_point tr_subspace_e3 = tr_space_e3;

		fixed_point bl_subspace_e1 = bl_space_e1;
		fixed_point bl_subspace_e2 = bl_space_e2;
		fixed_point bl_subspace_e3 = bl_space_e3;

		fixed_point br_subspace_e1 = br_space_e1;
		fixed_point br_subspace_e2 = br_space_e2;
		fixed_point br_subspace_e3 = br_space_e3;

		for(size_t by = 0; by < block_count_y; ++by){
			#define TEST_BLOCK(edge) \
				int edge##_type; \
				{\
					bool e_tl = tl_subspace_##edge > 0; \
					bool e_tr = tr_subspace_##edge > 0; \
					bool e_bl = bl_subspace_##edge > 0; \
					bool e_br = br_subspace_##edge > 0; \
					edge##_type = (e_tl << 0) | (e_tr << 1) | (e_bl << 2) | (e_br << 3); \
				}\

			TEST_BLOCK(e1);
			TEST_BLOCK(e2);
			TEST_BLOCK(e3);

			#undef TEST_BLOCK

			if(e1_type == 0 || e2_type == 0 || e3_type == 0)
				goto CONT;
			else if(e1_type == 0xF && e2_type == 0xF && e3_type == 0xF){
				Point sub_tl = {bounds.min_x + bx * BLOCK_SIZE, bounds.min_y + by * BLOCK_SIZE};
				Point sub_br = {sub_tl.x + BLOCK_SIZE, sub_tl.y + BLOCK_SIZE};
				FillFixedRectangle<fixed_point>(sub_tl.x, sub_tl.y, sub_br.x, sub_br.y, color);
			}else{
				fixed_point space_e1 = tl_subspace_e1;
				fixed_point space_e2 = tl_subspace_e2;
				fixed_point space_e3 = tl_subspace_e3;

				Point sub_tl = {bounds.min_x + bx * BLOCK_SIZE, bounds.min_y + by * BLOCK_SIZE};
				for(fixed_point x = sub_tl.x; x < sub_tl.x + BLOCK_SIZE; x += 1){
					fixed_point subspace_e1 = space_e1;
					fixed_point subspace_e2 = space_e2;
					fixed_point subspace_e3 = space_e3;

					for(fixed_point y = sub_tl.y; y < sub_tl.y + BLOCK_SIZE; y += 1){
						if(subspace_e1 > 0 && subspace_e2 > 0 && subspace_e3 > 0){
							PC::Plot((int) x.round(), (int) y.round(), color);
						}

						subspace_e1 += deltax_e1;
						subspace_e2 += deltax_e2;
						subspace_e3 += deltax_e3;
					}

					space_e1 -= deltay_e1;
					space_e2 -= deltay_e2;
					space_e3 -= deltay_e3;
				}
			}

		CONT:
			tl_subspace_e1 += block_deltax_e1;
			tl_subspace_e2 += block_deltax_e2;
			tl_subspace_e3 += block_deltax_e3;

			tr_subspace_e1 += block_deltax_e1;
			tr_subspace_e2 += block_deltax_e2;
			tr_subspace_e3 += block_deltax_e3;

			bl_subspace_e1 += block_deltax_e1;
			bl_subspace_e2 += block_deltax_e2;
			bl_subspace_e3 += block_deltax_e3;

			br_subspace_e1 += block_deltax_e1;
			br_subspace_e2 += block_deltax_e2;
			br_subspace_e3 += block_deltax_e3;
		}

		tl_space_e1 -= block_deltay_e1;
		tl_space_e2 -= block_deltay_e2;
		tl_space_e3 -= block_deltay_e3;

		tr_space_e1 -= block_deltay_e1;
		tr_space_e2 -= block_deltay_e2;
		tr_space_e3 -= block_deltay_e3;

		bl_space_e1 -= block_deltay_e1;
		bl_space_e2 -= block_deltay_e2;
		bl_space_e3 -= block_deltay_e3;

		br_space_e1 -= block_deltay_e1;
		br_space_e2 -= block_deltay_e2;
		br_space_e3 -= block_deltay_e3;
	}
}

void CopyTexture(araw::image& texture, int x, int y){
	size_t width;
	if(x + texture.header.width > PC_DISPLAY_WIDTH)
		width = x + texture.header.width - PC_DISPLAY_WIDTH;
	else
		width = texture.header.width;

	size_t height;
	if(y + texture.header.height > PC_DISPLAY_HEIGHT)
		height = y + texture.header.height - PC_DISPLAY_HEIGHT;
	else
		height = texture.header.height;

	for(size_t i = 0; i < height; ++i){
		uint8_t *row = PC::PixelAt(x, (int) (y + i));
		if(row == nullptr)
			break;

		std::memcpy(row, &texture.data[i * texture.header.width], width);
	}
}


struct Sector{
	double floor, ceil;
	std::vector<Point2>  vertices;
	std::vector<Sector*> edges;
};

#define FORCE_NEAR 0.01
float Project(float coord, float z, float near){
	// Avoid coordinates behind the near plane from being projected
	if(z < FORCE_NEAR) return 0.0;

	return (coord * near) / z;
}

struct Point3{
	float x, y, z;

	void operator *=(const Matrix4<float>& matrix){
		Matrix4<float> tmp;
		tmp[0][3] = this->x;
		tmp[1][3] = this->y;
		tmp[2][3] = this->z;
		tmp[3][3] = 1;

		tmp *= matrix;

		this->x = tmp[0][3];
		this->y = tmp[1][3];
		this->z = tmp[2][3];
	}

	Point2 project(float near){
		return {Project(this->x, this->z, near), Project(this->y, this->z, near)};
	}

	Point2 ortho() const { return {this->x, this->y}; }
};

static std::vector<araw::image> images;
constexpr float PI = 3.14159265358979;
void MainLoop(){
	static float delta = 0.016;

	static struct{
		float x = 0, z = 0, y = 0, angle = PI;
	} player;

	static bool up = false, down = false, left = false, right = false;
	sf::Event ev;
	while(PC::window.pollEvent(ev)){
		switch(ev.type){
			case sf::Event::KeyPressed:
				switch(ev.key.code){
					case sf::Keyboard::Left: left = true; break;
					case sf::Keyboard::Right: right = true; break;
					case sf::Keyboard::Up: up = true; break;
					case sf::Keyboard::Down: down = true; break;
				}
				break;
			case sf::Event::KeyReleased:
				switch(ev.key.code){
					case sf::Keyboard::Left: left = false; break;
					case sf::Keyboard::Right: right = false; break;
					case sf::Keyboard::Up: up = false; break;
					case sf::Keyboard::Down: down = false; break;
				}
				break;
			case sf::Event::Closed:
				PC::window.close();
				break;
		}
	}

	if(left)  player.angle -= PI * delta;
	if(right) player.angle += PI * delta;
	if(up)   { player.z += std::cos(player.angle) * delta * 10; player.x += std::sin(player.angle) * delta * 10; }
	if(down) { player.z -= std::cos(player.angle) * delta * 10; player.x -= std::sin(player.angle) * delta * 10; }

	// Clear screen
	std::memset(PC::display, 0, PC_DISPLAY_WIDTH * PC_DISPLAY_HEIGHT);

	// Render triangle
	Matrix4<float> transform =
		  Matrix4<float>::tait_bryan(0.0, player.angle, 0.0)
		* Matrix4<float>::translate3d(-player.x, -player.y, -player.z);

	static Matrix4<float> project  = Matrix4<float>::projection(PI / 2, PC_DISPLAY_WIDTH / PC_DISPLAY_HEIGHT, 0.1, 1);
	static Matrix4<float> viewport = Matrix4<float>::viewport(0, PC_DISPLAY_WIDTH, 0, PC_DISPLAY_HEIGHT, 0.1, 100);

	// Render towards triangle
	struct Triangle{
		Point3 a, b, c;

		void Plot(const Matrix4<float>& viewport, uint8_t color){
			Point2 aa = a.project(1), ba = b.project(1), ca = c.project(1);
			aa *= viewport;
			ba *= viewport;
			ca *= viewport;
			printf("Drawing triangle at {%.2f, %f}, {%f, %f}, {%f, %f}\n", aa.x, aa.y, ba.x, ba.y, ca.x, ca.y);
			DrawTriangle(aa, ba, ca, color);
		}

		void operator *=(const Matrix4<float>& matrix){
			this->a *= matrix;
			this->b *= matrix;
			this->c *= matrix;
		}
	};

	Point3 a = {0.5, -0.5, 2.0}, b = {-0.5, -0.5, 2.0}, c = {0.0, 0.5, 2.0};
	a *= transform;
	b *= transform;
	c *= transform;

	a *= project;
	b *= project;
	c *= project;

	if(a.z < -1 && b.z < -1 && c.z < -1){
		Point2 aa = a.ortho(), ba = b.ortho(), ca = c.ortho();
		aa *= viewport;
		ba *= viewport;
		ca *= viewport;

		printf("Drawing triangle {{%.2f, %.2f, %.2f}, {%.2f, %.2f, %.2f}, {%.2f, %.2f, %.2f}} at {{%.2f, %.2f}, {%.2f, %.2f}, {%.2f, %.2f}}\n", a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z, aa.x, aa.y, ba.x, ba.y, ca.x, ca.y);
		DrawTriangle(aa, ba, ca, 0xf);
	}

	Triangle towards = {{player.x, 0, static_cast<float>(0.8 + player.z)}, {player.x, 0,  static_cast<float>(0.8 + player.z)}, {player.x + std::cos(player.angle) * 2, 0, player.z + std::sin(player.angle) * 2}};
	towards *= transform;
	towards.Plot(viewport, 0x8);
}

int main(int argc, const char * argv[]){
	// insert code here...
	std::cout << "Hello, World!\n";

	std::ifstream source = std::ifstream("Resources/Textures/flora.256", std::ios_base::in | std::ios_base::binary);
	images.push_back(araw::image(source));

	PC::Init();
	PC::Run(MainLoop);

	TestMatrices();

    return 0;
}
