//
//  math.hh - Mathematical structures
//  Hallways
//
//  Created by Matheus Borella on 18/07/17.
//  Copyright © 2017 Matheus Borella. All rights reserved.
//

#ifndef __math_hh__
#define __math_hh__

#include <vector>
#include <stdexcept>

#include <cassert>

const static int  __LE_TEST_VALUE = 1;
const static bool IS_LITTLE_ENDIAN = (*((char*) &(__LE_TEST_VALUE)) == 1);

template <typename T>
static void flip_bytes(T *value){
	uint8_t *target = (uint8_t*) value;
	size_t size = sizeof(T);

	for(size_t i = 0; i < size / 2; ++i){
		uint8_t tmp = target[i];
		target[i] = target[size - i - 1];
		target[size - i - 1] = tmp;
	}
}

template <typename T = int, uint8_t _frac = 8>
class FixedPoint{
protected:
	T value;

	constexpr T fraction_mask() const{
		return static_cast<T>((static_cast<T>(1) << _frac) - static_cast<T>(1));
	}

	constexpr T whole_mask() const{
		return static_cast<T>((static_cast<T>(0) - static_cast<T>(1)) ^ fraction_mask());
	}

	constexpr T half() const{
		return static_cast<T>((static_cast<T>(1) << _frac) / static_cast<T>(2));
	}

	FixedPoint<T, _frac>(T value){
		this->value = value;
	}
public:
	FixedPoint<T, _frac>(T whole, T fraction){
		this->value = (whole << _frac) | (fraction & fraction_mask());
	}

	FixedPoint<T, _frac>(double source){
		double whole;
		double fraction = std::modf(source, &whole);

		this->value  = static_cast<T>(whole) << _frac;
		this->value += static_cast<T>(fraction * static_cast<double>(static_cast<T>(1) << _frac));
	}

	FixedPoint<T, _frac>() { }

	T floor()    const{ return (this->value & this->whole_mask()) >> _frac; }
	T fraction() const{ return this->value & this->fraction_mask(); }

	int signum() const{
		if(this->value >= 0) return 1;
		else return -1;
	}

	T ceil() const{
		if(this->fraction() == 0)
			return this->floor();
		else
			return this->floor() + 1;
	}

	T round() const{
		if(this->fraction() >= this->half())
			return this->ceil();
		else
			return this->floor();
	}

	float as_double() const{
		return static_cast<double>(this->value >> _frac) + static_cast<double>(this->fraction()) / static_cast<double>(static_cast<T>(1) << _frac);
	}

	FixedPoint<T, _frac> operator +(const FixedPoint<T, _frac>& other) const{ return {this->value + other.value}; }
	FixedPoint<T, _frac> operator -(const FixedPoint<T, _frac>& other) const{ return FixedPoint<T, _frac>(this->value - other.value); }
	FixedPoint<T, _frac> operator *(const FixedPoint<T, _frac>& other) const{
		T fraction = this->fraction() * other.floor() + this->floor() * other.fraction();
		T whole = (this->floor() * other.floor()) << _frac;

		return {whole + fraction};
	}
	FixedPoint<T, _frac> operator +(const T& other) const{ return {this->value + (other << _frac)}; }
	FixedPoint<T, _frac> operator -(const T& other) const{ return {this->value - (other << _frac)}; }
	FixedPoint<T, _frac> operator *(const T& other) const{
		T fraction = this->fraction() * other;
		T whole = (this->floor() * other) << _frac;

		return {whole + fraction};
	}
	FixedPoint<T, _frac> operator >>(const size_t& ammount) const{ return {this->value >> ammount}; }
	FixedPoint<T, _frac> operator <<(const size_t& ammount) const{ return {this->value << ammount}; }

	bool operator >(const FixedPoint<T, _frac>& other) const{ return this->value > other.value; }
	bool operator <(const FixedPoint<T, _frac>& other) const{ return this->value < other.value; }
	bool operator >(const T& other) const{ return this->value > other << _frac; }
	bool operator <(const T& other) const{ return this->value < other << _frac; }

	bool operator ==(const FixedPoint<T, _frac>& other) const{ return this->value == other.value; }
	bool operator ==(const T& other) const{ return this->value == other << _frac; }

	bool operator !=(const FixedPoint<T, _frac>& other) const{ return this->value != other.value; }
	bool operator !=(const T& other) const{ return this->value != other << _frac; }

	void operator +=(const FixedPoint<T, _frac>& other){ this->value += other.value; }
	void operator -=(const FixedPoint<T, _frac>& other){ this->value -= other.value; }
	void operator *=(const FixedPoint<T, _frac>& other){
		T fraction = this->fraction() * other.floor() + this->floor() * other.fraction();
		T whole = (this->floor() * other.floor()) << _frac;

		this->value = fraction + whole;
	}
	void operator +=(const T& other){ this->value += other << _frac; }
	void operator -=(const T& other){ this->value -= other << _frac; }
	void operator *=(const T& other){
		T fraction = this->fraction() * other;
		T whole = (this->floor() * other) << _frac;

		this->value = whole + fraction;
	}
	void operator >>=(const size_t& ammount){ this->value >>= ammount; }
	void operator <<=(const size_t& ammount){ this->value <<= ammount; }
};

template<typename T>
class Matrix4{
public:
	static Matrix4<T> identity(){
		return static_cast<Matrix4<T>>(Matrix4<double>({{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}}));
	}

	static Matrix4<T> translate2d(double x, double y){
		return static_cast<Matrix4<T>>(Matrix4<double>({{1, 0, 0, x}, {0, 1, 0, y}, {0, 0, 1, 0}, {0, 0, 0, 1}}));
	}

	static Matrix4<T> rotate2d(double angle){
		return static_cast<Matrix4<T>>(Matrix4<double>({
			{std::cos(angle), -std::sin(angle), 0, 0},
			{std::sin(angle),  std::cos(angle), 0, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 1}
		}));
	}

	static Matrix4<T> viewport(double left, double right, double top, double bottom, double near, double far){
		return static_cast<Matrix4<T>>(Matrix4<double>({
			{(right - left) / 2, 0, 0, (right + left) / 2},
			{0, (top - bottom) / 2, 0, (top + bottom) / 2},
			{0, 0, (far - near) / 2, (far + near) / 2},
			{0, 0, 0, 1}
		}));
	}

	static Matrix4<T> projection(double fov, double ratio, double near, double far){
		double d = 1.0 / std::tan(fov / 2);
		return static_cast<Matrix4<T>>(Matrix4<double>({
			{d / ratio, 0, 0, 0},
			{0, d, 0, 0},
			{0, 0, (near + far) / (near - far), (2 * near * far) / (near - far)},
			{0, 0, -1, 0}
		}));
	}

	static Matrix4<T> translate3d(double x, double y, double z){
		return static_cast<Matrix4<T>>(Matrix4<double>({{1, 0, 0, x}, {0, 1, 0, y}, {0, 0, 1, z}, {0, 0, 0, 1}}));
	}

	static Matrix4<T> rotate3d(double angle, bool x, bool y, bool z){
		return static_cast<Matrix4<T>>(Matrix4<double>({
			{
				x * x * (1 - std::cos(angle)) +     std::cos(angle),
				y * x * (1 - std::cos(angle)) - z * std::sin(angle),
				z * x * (1 - std::cos(angle)) + y * std::sin(angle),
				0
			},
			{
				x * y * (1 - std::cos(angle)) + z * std::sin(angle),
				y * y * (1 - std::cos(angle)) +     std::cos(angle),
				z * y * (1 - std::cos(angle)) - x * std::sin(angle),
				0
			},
			{
				x * z * (1 - std::cos(angle)) - y * std::sin(angle),
				y * z * (1 - std::cos(angle)) + x * std::sin(angle),
				z * z * (1 - std::cos(angle)) +     std::cos(angle),
				0
			},
			{0, 0, 0, 1},
		}));
	}

	static Matrix4<T> tait_bryan(double yaw, double pitch, double roll){
		Matrix4<T> yaw_matrix = Matrix4<double>({
			{1, 0, 0, 0},
			{0,  std::cos(yaw), std::sin(yaw), 0},
			{0, -std::sin(yaw), std::cos(yaw), 0},
			{0, 0, 0, 1}
		});

		Matrix4<T> pitch_matrix = Matrix4<double>({
			{std::cos(pitch), 0, -std::sin(pitch), 0},
			{0, 1, 0, 0},
			{std::sin(pitch), 0, std::cos(pitch), 0},
			{0, 0, 0, 1}
		});

		Matrix4<T> roll_matrix = Matrix4<double>({
			{ std::cos(roll), std::sin(roll), 0, 0},
			{-std::sin(roll), std::cos(roll), 0, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 1}
		});

		return yaw_matrix * pitch_matrix * roll_matrix;
	}

private:
	T values[4][4];

public:
	Matrix4<T>(){
		for(size_t i = 0; i < 4; ++i)
			for(size_t j = 0; j < 4; ++j)
				this->values[i][j] = 0.0f;
	}

	Matrix4<T>(std::initializer_list<std::initializer_list<T>> list){
		size_t i = 0;
		for(auto row = list.begin(); row != list.end(); ++row){
			size_t j = 0;
			for(auto item = row->begin(); item != row->end(); ++item){
				// Check if the list is larger than 4x4 in either direction
				if(i >= 4 && j >= 4)
					throw std::invalid_argument("List is too large");

				this->values[i][j] = *item;
				++j;
			}

			// Check width mismatch
			if(j < 3)
				throw std::invalid_argument("List is too thin");

			++i;
		}

		// Check height mismatch
		if(i < 3)
			throw std::invalid_argument("List is too short");
	}

	bool operator ==(const Matrix4<T>& other) const{
		for(size_t i = 0; i < 4; ++i){
			for(size_t j = 0; j < 4; ++j){
				if(this->values[i][j] != other.values[i][j])
					return false;
			}
		}

		return true;
	}

	Matrix4<T> operator *(const Matrix4<T>& other) const{
		Matrix4<T> matrix;
		for(size_t i = 0; i < 4; ++i){
			for(size_t j = 0; j < 4; ++j){
				matrix[i][j]  = this->values[i][0] * other.values[0][j];
				matrix[i][j] += this->values[i][1] * other.values[1][j];
				matrix[i][j] += this->values[i][2] * other.values[2][j];
				matrix[i][j] += this->values[i][3] * other.values[3][j];
			}
		}

		return matrix;
	}

	Matrix4<T> operator *(const T& scalar) const{
		Matrix4<T> matrix = *this;
		for(size_t i = 0; i < 4; ++i){
			for(size_t j = 0; j < 4; ++j){
				matrix[i][j] *= scalar;
			}
		}

		return matrix;
	}

	Matrix4<T> operator +(const Matrix4<T>& other) const{
		Matrix4<T> matrix = *this;
		for(size_t i = 0; i < 4; ++i){
			for(size_t j = 0; j < 4; ++j)
				matrix[i][j] += other[i][j];
		}

		return matrix;
	}

	Matrix4<T> operator -(const Matrix4<T>& other) const{
		Matrix4<T> matrix = *this;
		for(size_t i = 0; i < 4; ++i){
			for(size_t j = 0; j < 4; ++j)
				matrix[i][j] -= other[i][j];
		}

		return matrix;
	}
	void operator *=(const Matrix4<T>& other){
		*this = other * *this;
	}

	void operator +=(const Matrix4<T>& other){
		*this = other + *this;
	}

	void operator -=(const Matrix4<T>& other){
		*this = other - *this;
	}

	T* operator[](const size_t& i){
		return this->values[i];
	}

	const T* operator[](const size_t& i) const{
		return this->values[i];
	}

	template<typename U>
	operator Matrix4<U>(){
		Matrix4<U> matrix;
		for(size_t i = 0; i < 4; ++i){
			for(size_t j = 0; j < 4; ++j)
				matrix[i][j] = static_cast<U>(this->values[i][j]);
		}

		return matrix;
	}

	void print(){
		for(size_t i = 0; i < 4; ++i){
			std::cout << "| ";
			for(size_t j = 0; j < 4; ++j)
				printf("%*.2f ", 10, this->values[i][j]);

			std::cout << "|" << std::endl;
		}
	}
};

void TestMatrices(){
	printf("Testing matrices... ");

	Matrix4<float> identity = Matrix4<float>::identity();
	Matrix4<float> a = {
		{1, 2, 3, 4},
		{4, 3, 2, 1},
		{3, 4, 2, 1},
		{1, 2, 4, 3}
	};

	assert(a * identity == a);

	printf("ok!\n");
}

template<typename T>
class Vector4{
private:
	T values[4];

public:
	Vector4(){ this->values = {{0}}; }

	Vector4<T>(std::initializer_list<T> list){
		size_t i = 0;
		for(auto item = list.begin(); item != list.end(); ++item){
			this->values[i] = *item;
			++i;
		}

		// Check height mismatch
		if(i < 3)
			throw std::invalid_argument("List is too short");
	}

	T& operator [](const size_t& i){
		return this->values[i];
	}

	const T& operator [](const size_t& i) const {
		return this->values[i];
	}

	Vector4<T> operator +(const Vector4<T>& other){
		Vector4<T> vec = *this;
		for(size_t i = 0; i < 4; ++i)
			vec[i] += other[i];

		return vec;
	}

	Vector4<T> operator -(const Vector4<T>& other){
		Vector4<T> vec = *this;
		for(size_t i = 0; i < 4; ++i)
			vec[i] -= other[i];

		return vec;
	}

	void operator +=(const Vector4<T>& other){
		*this = *this + other;
	}

	void operator -=(const Vector4<T>& other){
		*this = *this - other;
	}

	operator Matrix4<T>(){
		Matrix4<T> matrix;
		matrix[0][3] = this->values[0];
		matrix[1][3] = this->values[1];
		matrix[2][3] = this->values[2];
		matrix[3][3] = this->values[3];

		return matrix;
	}
};
#endif /* matrix_h */
